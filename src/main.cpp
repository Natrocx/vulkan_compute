#include <iostream>
#include <chrono>
#include <optional>
#include <vector>
#include <vulkan/vulkan.hpp>
#include "../external/patterns/include/mpark/patterns.hpp"
#include <glm/glm.hpp>
#include <jpeglib.h>

using namespace mpark::patterns;

int32_t WORKGROUP_SIZE = 32; // Workgroup size in compute shader.

const bool enableValidationLayers = true;

VkInstance instance;

#define VK_CHECK_RESULT(f) { \
    VkResult result = (f);                   \
    match(result)(                \
            pattern(VK_SUCCESS) = [] {},\
            pattern(_) = [&] {\
                std::printf("An error occured in a Vulkan function call in %s at line %d: %d", __FILE__, __LINE__, result);\
            }\
            );\
}
typedef glm::vec<4, double, glm::defaultp> dvec4;
typedef glm::vec<2, double, glm::defaultp> dvec2;

struct PushData {
    dvec4 dimensions;
    uint32_t width, height;
};

struct Pixel {
    float r, g, b, a;
};

class ComputeApplication {
public:


    void run() {
        createInstance();
        findPhysicalDevice();
        createDevice();

        configurePushData();
        createStorageBuffer();
        createDescriptorSet();
        createComputePipeline();
        createCommandBuffer();

        recordCommandBuffer();
        runCommands();

        writeImage();
        cleanup();
    }


private:

    VkInstance instance;
    VkDebugReportCallbackEXT debugReportCallback;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    uint32_t queueFamilyIndex;
    VkQueue queue;
    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;
    VkShaderModule shaderModule;
    VkDescriptorSetLayout descriptorSetLayout;
    VkDescriptorPool descriptorPool;
    uint64_t bufferSize;

    PushData pushData;
    std::vector<const char *> enabledExtensions;
    std::vector<const char *> enabledLayers;

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
            VkDebugReportFlagsEXT flags,
            VkDebugReportObjectTypeEXT objectType,
            uint64_t object,
            size_t location,
            int32_t messageCode,
            const char *pLayerPrefix,
            const char *pMessage,
            void *pUserData) {

        printf("Debug Report: %s: %s\n", pLayerPrefix, pMessage);

        return VK_FALSE;
    }

    void createInstance() {
        /*
        By enabling validation layers, Vulkan will emit warnings if the API
        is used incorrectly. VK_LAYER_KHRONOS_validation is the Layer of choice
        in newer Vulkan versions. All other validation Layers have been deprecated.
        */

        if (enableValidationLayers) {
            /*
            We get all supported layers with vkEnumerateInstanceLayerProperties.
            */
            uint32_t layerCount;
            vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

            std::vector<VkLayerProperties> layerProperties(layerCount);
            vkEnumerateInstanceLayerProperties(&layerCount, layerProperties.data());

            /*
            And then we simply check if  VK_LAYER_KHRONOS_validation is among the supported layers.
            */
            bool foundLayer = false;
            for (VkLayerProperties prop : layerProperties) {
                if (strcmp("VK_LAYER_KHRONOS_validation", prop.layerName) == 0) {
                    foundLayer = true;
                    break;
                }
            }

            if (!foundLayer) {
                throw std::runtime_error("Layer VK_LAYER_KHRONOS_validation not supported\n");
            }
            enabledLayers.push_back("VK_LAYER_KHRONOS_validation"); // Alright, we can use this layer.
        }
        /*
            We need to enable an extension named VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
            in order to be able to print the warnings emitted by the validation layer.

            So again, we just check if the extension is among the supported extensions.
            */

        uint32_t extensionCount;

        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
        std::vector<VkExtensionProperties> extensionProperties(extensionCount);
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensionProperties.data());

        uint32_t foundExtension = 0;
        for (VkExtensionProperties prop : extensionProperties) {
            if (strcmp(VK_EXT_DEBUG_REPORT_EXTENSION_NAME, prop.extensionName) == 0) {
                foundExtension |= 0b1u;
            }
        }

        // if this is a release build, do not require the debug extension
        if (!enableValidationLayers) {
            foundExtension |= 0b1u;
        }

        if (foundExtension != 0b1) {
            std::cout << "Not all necessary extensions are supported. Required extensions are:\n"
                         "VK_EXT_debug_report \n";

            std::cout << "Found extensions: \n";

            for (VkExtensionProperties prop : extensionProperties) {
                std::cout << prop.extensionName << "\n";
            }

            throw std::runtime_error("");
        }

        if (enableValidationLayers) {
            enabledExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        }

        /*
        Contains application info. This is actually not that important.
        The only real important field is apiVersion.
        */
        VkApplicationInfo applicationInfo = {};
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = "Hello world app";
        applicationInfo.applicationVersion = 0;
        applicationInfo.pEngineName = "awesomeengine";
        applicationInfo.engineVersion = 0;
        applicationInfo.apiVersion = VK_API_VERSION_1_2;

        VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.flags = 0;
        createInfo.pApplicationInfo = &applicationInfo;

        // Give our desired layers and extensions to vulkan.
        createInfo.enabledLayerCount = enabledLayers.size();
        createInfo.ppEnabledLayerNames = enabledLayers.data();
        createInfo.enabledExtensionCount = (uint32_t) enabledExtensions.size();
        createInfo.ppEnabledExtensionNames = enabledExtensions.data();

        /*
        Actually create the instance.
        Having created the instance, we can actually start using vulkan.
        */
        VK_CHECK_RESULT(vkCreateInstance(
                &createInfo,
                nullptr,
                &instance));
        /*
        Register a callback function for the extension VK_EXT_DEBUG_REPORT_EXTENSION_NAME, so that warnings emitted from the validation
        layer are actually printed.
        */
        if (enableValidationLayers) {
            VkDebugReportCallbackCreateInfoEXT createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
            createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT |
                               VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
            createInfo.pfnCallback = &debugReportCallbackFn;

            // We have to explicitly load this function.
            auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance,
                                                                                                             "vkCreateDebugReportCallbackEXT");
            if (vkCreateDebugReportCallbackEXT == nullptr) {
                throw std::runtime_error("Could not load vkCreateDebugReportCallbackEXT");
            }

            // Create and register callback.
            VK_CHECK_RESULT(vkCreateDebugReportCallbackEXT(instance, &createInfo, nullptr, &debugReportCallback));
        }
    }

    void findPhysicalDevice() {
        /*
        In this function, we find a physical device that can be used with Vulkan.
        */

        /*
        So, first we will list all physical devices on the system with vkEnumeratePhysicalDevices .
        */
        uint32_t deviceCount;
        vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
        if (deviceCount == 0) {
            throw std::runtime_error("could not find a device with vulkan support");
        }

        std::vector<VkPhysicalDevice> devices(deviceCount);
        vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

        physicalDevice = devices.front();

        // clear this to store device extensions
        enabledExtensions.clear();


        for (VkPhysicalDevice physDevice : devices) {
            // for now, no extensions are needed so there is no check for extensions


            if (checkPhysicalDeviceFeatures(physDevice)) {
                physicalDevice = physDevice;
                break;
            }

        }
    }

    uint32_t getComputeQueueFamilyIndex() {
        uint32_t queueFamilyCount;

        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

        // Retrieve all queue families.
        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

        // Now find a family that supports compute.
        uint32_t i = 0;
        for (; i < queueFamilies.size(); ++i) {
            VkQueueFamilyProperties props = queueFamilies[i];

            if (props.queueCount > 0 && (props.queueFlags & VK_QUEUE_COMPUTE_BIT)) {
                // found a queue with compute. We're done!
                break;
            }
        }

        if (i == queueFamilies.size()) {
            throw std::runtime_error("could not find a queue family that supports operations");
        }

        return i;
    }

    void createDevice() {
        /*
        We create the logical device in this function.
        */

        /*
        When creating the device, we also specify what queues it has.
        */
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamilyIndex = getComputeQueueFamilyIndex();
        queueCreateInfo.queueCount = 1; // create one queue in this family. We don't need more.
        float queuePriorities = 1.0;  // we only have one queue, so this is not that imporant.
        queueCreateInfo.pQueuePriorities = &queuePriorities;

        /*
        Now we create the logical device. The logical device allows us to interact with the physical
        device.
        */
        VkDeviceCreateInfo deviceCreateInfo = {};

        // the features here have been checked for during physical device creation
        // and can therefore be enabled without further checks
        VkPhysicalDeviceFeatures deviceFeatures{};
        deviceFeatures.shaderFloat64 = VK_TRUE;
        deviceFeatures.shaderInt16 = VK_TRUE;

        deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCreateInfo.enabledLayerCount = enabledLayers.size();  // need to specify validation layers here as well.
        deviceCreateInfo.ppEnabledLayerNames = enabledLayers.data();
        deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo; // when creating the logical device, we also specify what queues it has.
        deviceCreateInfo.queueCreateInfoCount = 1;
        deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

        VK_CHECK_RESULT(vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device)); // create logical device.

        // Get a handle to the only member of the queue family.
        vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
    }

    void configurePushData() {
        pushData.dimensions = dvec4{-2.1, 4.26, -1.2, 2.4};

        pushData.width = 10000;
        pushData.height = static_cast<uint32_t>((static_cast<double>(pushData.width) / 16.0 * 9.0));
        printf("Using image size %ux%u \n", pushData.width, pushData.height);
    }

    static bool checkPhysicalDeviceFeatures(VkPhysicalDevice dev) {

        VkPhysicalDeviceFeatures2 pDF{
                .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
                .pNext = nullptr
        };
        vkGetPhysicalDeviceFeatures2(dev, &pDF);

        return pDF.features.shaderFloat64 & pDF.features.shaderInt16;
    }

    // find memory type with desired properties.
    uint32_t findMemoryType(uint32_t memoryTypeBits, VkMemoryPropertyFlags properties) {
        VkPhysicalDeviceMemoryProperties memoryProperties;

        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /*
        How does this search work?
        See the documentation of VkPhysicalDeviceMemoryProperties for a detailed description.
        */
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i) {
            if ((memoryTypeBits & (1 << i)) &&
                ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties))
                return i;
        }
        return -1;
    }

    // Read file into array of bytes, and cast to uint32_t*, then return.
    // The data has been padded, so that it fits into an array uint32_t.
    static uint32_t *readFile(uint32_t &length, const char *filename) {

        FILE *fp = fopen(filename, "rb");
        if (fp == nullptr) {
            printf("Could not find or open file: %s\n", filename);
        }

        // get file size.
        fseek(fp, 0, SEEK_END);
        long filesize = ftell(fp);
        fseek(fp, 0, SEEK_SET);

        long filesizePadded = static_cast<long>(ceil(static_cast<double>(filesize) / 4.0)) * 4;

        // read file contents.
        char *str = new char[filesizePadded];
        fread(str, filesize, sizeof(char), fp);
        fclose(fp);

        // data padding.
        for (long i = filesize; i < filesizePadded; i++) {
            str[i] = 0;
        }

        length = filesizePadded;
        return reinterpret_cast<uint32_t *>(str);
    }

    void createStorageBuffer() {
        bufferSize = pushData.width * pushData.height * sizeof(Pixel);
        VkBufferCreateInfo bufferCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .pNext = nullptr,
                .flags = 0,
                .size = bufferSize,
                .usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &queueFamilyIndex
        };
        VK_CHECK_RESULT(vkCreateBuffer(device, &bufferCreateInfo, nullptr, &storageBuffer));

        // allocate buffer:
        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(device, storageBuffer, &memoryRequirements);

        VkMemoryAllocateInfo memoryAllocateInfo = {
                .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                .allocationSize = memoryRequirements.size,
                .memoryTypeIndex = findMemoryType(memoryRequirements.memoryTypeBits,
                                                  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                                  VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
        };

        VK_CHECK_RESULT(vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &bufferMemory));

        VK_CHECK_RESULT(vkBindBufferMemory(device, storageBuffer, bufferMemory, 0));
    }

    void createDescriptorSet() {
        VkDescriptorSetLayoutBinding descriptorSetLayoutBinding = {
                .binding = 0,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT
        };

        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                .pNext = nullptr,
                .flags = 0,
                .bindingCount = 1,
                .pBindings = &descriptorSetLayoutBinding
        };

        VkDescriptorSetLayoutSupport layoutSupport = {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT,
                .pNext = nullptr
        };

        vkGetDescriptorSetLayoutSupport(device, &descriptorSetLayoutCreateInfo, &layoutSupport);
        if (!layoutSupport.supported)
            throw std::runtime_error("DescriptorSetLayout not supported. Can't continue.");

        // Create the descriptor set layout.
        VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, nullptr,
                                                    &descriptorSetLayout));


        // we do not need to bind more than one object (in this case a buffer)
        VkDescriptorPoolSize descriptorPoolSize = {
                .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .descriptorCount = 1
        };

        VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
                .pNext = nullptr,
                .maxSets = 1,
                .poolSizeCount = 1,
                .pPoolSizes = &descriptorPoolSize,
        };

        // create descriptor pool.
        VK_CHECK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, nullptr, &descriptorPool));

        /*
        With the pool allocated, we can now allocate the descriptor set.
        */
        VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                .pNext = nullptr,
                .descriptorPool = descriptorPool,
                .descriptorSetCount = 1,
                .pSetLayouts = &descriptorSetLayout
        };

        VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &descriptorSet));

        VkDescriptorBufferInfo descriptorBufferInfo = {
                .buffer = storageBuffer,
                .offset = 0,
                .range = bufferSize
        };

        VkWriteDescriptorSet writeDescriptorSet = {
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptorSet,
                .dstBinding = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .pBufferInfo = &descriptorBufferInfo
        };
        VkResult res;

        // does not return a VkResult
        vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, nullptr);
    }

    void createComputePipeline() {
        /*
       We create a compute pipeline here.
       */

        /*
        Create a shader module. A shader module basically just encapsulates some shader code.
        */
        uint32_t filelength;
        // the code in comp.spv was created by running the command:
        // glslangValidator.exe -V shader.comp
        uint32_t *code = readFile(filelength, "./shaders/mandelbrot_buffer.spv");
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.pCode = code;
        createInfo.codeSize = filelength;

        VK_CHECK_RESULT(vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule));
        delete[] code;

        /*
        Now let us actually create the compute pipeline.
        A compute pipeline is very simple compared to a graphics pipeline.
        It only consists of a single stage with a compute shader.

        So first we specify the compute shader stage, and it's entry point(main).
        */
        VkPipelineShaderStageCreateInfo shaderStageCreateInfo = {};
        shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCreateInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
        shaderStageCreateInfo.module = shaderModule;
        shaderStageCreateInfo.pName = "main";

        // meta info for push constants used to pass target swapchain image and drawing region as well as resolution
        VkPushConstantRange pushConstantRange{
                .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
                .offset = 0,
                .size = sizeof(PushData)};

        /*
        The pipeline layout allows the pipeline to access descriptor sets.
        So we just specify the descriptor set layout we created earlier.
        */
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                .setLayoutCount = 1,
                .pSetLayouts = &descriptorSetLayout,
                .pushConstantRangeCount = 1,
                .pPushConstantRanges = &pushConstantRange};
        VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

        VkComputePipelineCreateInfo pipelineCreateInfo = {};
        pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        pipelineCreateInfo.stage = shaderStageCreateInfo;
        pipelineCreateInfo.layout = pipelineLayout;

        /*
        Now, we finally create the compute pipeline.
        */
        VK_CHECK_RESULT(vkCreateComputePipelines(
                device, VK_NULL_HANDLE,
                1, &pipelineCreateInfo,
                nullptr, &pipeline));
    }

    /// creates command pool and allocates command Buffer
    void createCommandBuffer() {
        VkCommandPoolCreateInfo commandPoolCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .pNext = nullptr,
                .flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT
        };
        VK_CHECK_RESULT(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool));

        VkCommandBufferAllocateInfo commandBufferAllocateInfo = {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .pNext = nullptr,
                .commandPool = commandPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1
        };

        VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer));

    }

    VkDescriptorSet descriptorSet;
    VkBuffer storageBuffer;
    VkDeviceMemory bufferMemory;
    VkCommandPool commandPool;
    VkCommandBuffer commandBuffer;


    /// record command buffer
    void recordCommandBuffer() {
        VkCommandBufferBeginInfo commandBufferBeginInfo = {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                .pNext = nullptr,
                .flags = 0,
                .pInheritanceInfo = nullptr
        };

        VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo));

        // push information about the area of interest
        vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(PushData), &pushData);
        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSet, 0,
                                nullptr);

        // execute previous commands on submission
        vkCmdDispatch(commandBuffer,
                      static_cast<uint32_t>(ceil(
                              static_cast<double>(pushData.width) / static_cast<double>(WORKGROUP_SIZE))),
                      static_cast<uint32_t>(ceil(
                              static_cast<double>(pushData.height) / static_cast<double>(WORKGROUP_SIZE))),
                      1
        );
        VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffer));
    }

    void runCommands() {
        // there is no need to use any semaphores here and thus we dont
        VkSubmitInfo submitInfo = {
                .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                .pNext = nullptr,
                .waitSemaphoreCount = 0,
                .pWaitSemaphores = nullptr,
                .pWaitDstStageMask = nullptr,
                .commandBufferCount = 1,
                .pCommandBuffers = &commandBuffer,
                .signalSemaphoreCount = 0,
                .pSignalSemaphores = nullptr
        };

        // create a fence which will be used to await computation results
        VkFenceCreateInfo fenceCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
                .pNext = nullptr,
                .flags = 0
        };
        VK_CHECK_RESULT(vkCreateFence(device, &fenceCreateInfo, nullptr, &computeFence));

        auto begin = std::chrono::steady_clock::now();
        // submit commands for execution and submit the fence to await results
        VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, computeFence));

        // since there is nothing else to do meanwhile, await results here
        VK_CHECK_RESULT(vkWaitForFences(device, 1, &computeFence, VK_TRUE, UINT64_MAX));
        auto end = std::chrono::steady_clock::now();

        printf("Frametime of %ld microseconds \n",
               std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count());

        vkDestroyFence(device, computeFence, nullptr);
    }

    void writeImage() {
        auto remap_begin = std::chrono::steady_clock::now();
        void *mappedMemory = nullptr;
        vkMapMemory(device, bufferMemory, 0, bufferSize, 0, &mappedMemory);
        auto *pmappedMemory = (Pixel *) mappedMemory;

        // Get the color data from the buffer, and cast it to bytes.
        // We save the data to a vector.
        std::vector<unsigned char> image;
        image.reserve(pushData.width * pushData.height * 3);
        for (int i = 0; i < pushData.width * pushData.height; i += 1) {
            image.push_back((unsigned char) (255.0f * (pmappedMemory[i].r)));
            image.push_back((unsigned char) (255.0f * (pmappedMemory[i].g)));
            image.push_back((unsigned char) (255.0f * (pmappedMemory[i].b)));
        }
        // Done reading, so unmap.
        vkUnmapMemory(device, bufferMemory);

        auto remap_end = std::chrono::steady_clock::now();

        printf("Remapping image data took %ld microseconds\n",
               std::chrono::duration_cast<std::chrono::microseconds>(remap_end - remap_begin).count());


        auto begin = std::chrono::steady_clock::now();
        // Now we save the acquired color data to a jpeg
        struct jpeg_compress_struct cinfo;
        struct jpeg_error_mgr jerr{};
        auto file = fopen("./mandelbrot.jpg", "w");
        if (file == nullptr) {
            fprintf(stderr, "can't open %s\n", "./mandelbrot.jpg");
            exit(1);
        };
        cinfo.err = jpeg_std_error(&jerr);
        jpeg_create_compress(&cinfo);
        jpeg_stdio_dest(&cinfo, file);
        cinfo.image_width = pushData.width;
        cinfo.image_height = pushData.height;
        cinfo.input_components = 3;
        cinfo.in_color_space = JCS_RGB;

        jpeg_set_defaults(&cinfo);
        jpeg_set_quality(&cinfo, 100, true);

        jpeg_start_compress(&cinfo, true);
        uint32_t row_stride = pushData.width * 3;
        JSAMPROW row_pointer[1];
        while (cinfo.next_scanline < cinfo.image_height) {
            row_pointer[0] = &image.at(cinfo.next_scanline * row_stride);
            (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
        }
        jpeg_finish_compress(&cinfo);
        fclose(file);
        jpeg_destroy_compress(&cinfo);
        auto end = std::chrono::steady_clock::now();

        printf("Writing image file took %ld microseconds\n",
               std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count());
    }

    void cleanup() {
        /*
        Clean up all Vulkan Resources.
        */

        if (enableValidationLayers) {
            // destroy callback.
            auto func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance,
                                                                                    "vkDestroyDebugReportCallbackEXT");
            if (func == nullptr) {
                throw std::runtime_error("Could not load vkDestroyDebugReportCallbackEXT");
            }
            func(instance, debugReportCallback, nullptr);
        }

        vkFreeMemory(device, bufferMemory, nullptr);
        vkDestroyBuffer(device, storageBuffer, nullptr);
        vkDestroyShaderModule(device, shaderModule, nullptr);
        vkDestroyDescriptorPool(device, descriptorPool, nullptr);
        vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
        vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
        vkDestroyPipeline(device, pipeline, nullptr);
        vkDestroyCommandPool(device, commandPool, nullptr);
        vkDestroyDevice(device, nullptr);
        vkDestroyInstance(instance, nullptr);
    }

    VkFence computeFence;
};


int main() {
    ComputeApplication app;

    try {
        app.run();
    }
    catch (const std::runtime_error &e) {
        printf("%s\n", e.what());
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
